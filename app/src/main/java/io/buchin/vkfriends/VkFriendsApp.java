package io.buchin.vkfriends;

import android.app.Application;

import com.vk.sdk.VKSdk;

import io.buchin.vkfriends.common.di.AppModule;

public class VkFriendsApp extends Application {
    public AppModule appModule = new AppModule();

    @Override
    public void onCreate() {
        super.onCreate();
        VKSdk.initialize(this);
    }
}
