package io.buchin.vkfriends.models;

public class Friend {
    private String friendName = "";
    private String friendSurname = "";
    private String friendPhotoMax = "";
    private String friendBdate = "";

    public Friend(String friendName, String friendSurname, String friendPhotoMax, String friendBdate) {
        this.friendName = friendName;
        this.friendSurname = friendSurname;
        this.friendPhotoMax = friendPhotoMax;
        this.friendBdate = friendBdate;
    }

    public String getFriendName() {
        return friendName;
    }

    public void setFriendName(String friendName) {
        this.friendName = friendName;
    }

    public String getFriendSurname() {
        return friendSurname;
    }

    public void setFriendSurname(String friendSurname) {
        this.friendSurname = friendSurname;
    }

    public String getFriendPhotoMax() {
        return friendPhotoMax;
    }

    public void setFriendPhotoMax(String friendPhotoMax) {
        this.friendPhotoMax = friendPhotoMax;
    }

    public String getFriendBdate() {
        return friendBdate;
    }

    public void setFriendBdate(String friendBdate) {
        this.friendBdate = friendBdate;
    }

    @Override
    public String toString() {
        return "Friend{" +
                "friendName='" + friendName + '\'' +
                ", friendSurname='" + friendSurname + '\'' +
                ", friendPhotoMax='" + friendPhotoMax + '\'' +
                ", friendBdate='" + friendBdate + '\'' +
                '}';
    }
}
