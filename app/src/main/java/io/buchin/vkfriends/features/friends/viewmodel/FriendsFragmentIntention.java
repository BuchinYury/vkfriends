package io.buchin.vkfriends.features.friends.viewmodel;

public class FriendsFragmentIntention {
    public static final ScreenResumed screenResumed = new ScreenResumed();
    public static final RetryButtonClicked retryButtonClicked = new RetryButtonClicked();
    public static final OpenFiltersButtonClicked openFiltersButtonClicked =
            new OpenFiltersButtonClicked();
    public static final CloseFiltersButtonClicked closeFiltersButtonClicked =
            new CloseFiltersButtonClicked();

    private static class ScreenResumed extends FriendsFragmentIntention {
    }

    private static class OpenFiltersButtonClicked extends FriendsFragmentIntention {
    }

    private static class CloseFiltersButtonClicked extends FriendsFragmentIntention {
    }

    private static class RetryButtonClicked extends FriendsFragmentIntention {
    }

    public static class FilterNopeCheckedChange extends FriendsFragmentIntention {
        private boolean isFilterNopeChecked;

        public FilterNopeCheckedChange(boolean isFilterNopeChecked) {
            this.isFilterNopeChecked = isFilterNopeChecked;
        }

        public boolean isFilterNopeChecked() {
            return isFilterNopeChecked;
        }
    }

    public static class FilterWithPhotoCheckedChange extends FriendsFragmentIntention {
        private boolean isFilterWithPhotoChecked;

        public FilterWithPhotoCheckedChange(boolean isFilterWithPhotoChecked) {
            this.isFilterWithPhotoChecked = isFilterWithPhotoChecked;
        }

        public boolean isFilterWithPhotoChecked() {
            return isFilterWithPhotoChecked;
        }
    }

    public static class FilterWithoutPhotoCheckedChange extends FriendsFragmentIntention {
        private boolean isFilterWithoutPhotoChecked;

        public FilterWithoutPhotoCheckedChange(boolean isFilterWithoutPhotoChecked) {
            this.isFilterWithoutPhotoChecked = isFilterWithoutPhotoChecked;
        }

        public boolean isFilterWithoutPhotoChecked() {
            return isFilterWithoutPhotoChecked;
        }
    }

    public static class SortNopeCheckedChange extends FriendsFragmentIntention {
        private boolean isSortNopeChecked;

        public SortNopeCheckedChange(boolean isSortNopeChecked) {
            this.isSortNopeChecked = isSortNopeChecked;
        }

        public boolean isSortNopeChecked() {
            return isSortNopeChecked;
        }
    }

    public static class SortAscendingAgeCheckedChange extends FriendsFragmentIntention {
        private boolean isSortAscendingAgeChecked;

        public SortAscendingAgeCheckedChange(boolean isSortAscendingAgeChecked) {
            this.isSortAscendingAgeChecked = isSortAscendingAgeChecked;
        }

        public boolean isSortAscendingAgeChecked() {
            return isSortAscendingAgeChecked;
        }
    }

    public static class SortDescendingAgeCheckedChange extends FriendsFragmentIntention {
        private boolean isSortDescendingAgeChecked;

        public SortDescendingAgeCheckedChange(boolean isSortDescendingAgeChecked) {
            this.isSortDescendingAgeChecked = isSortDescendingAgeChecked;
        }

        public boolean isSortDescendingAgeChecked() {
            return isSortDescendingAgeChecked;
        }
    }
}
