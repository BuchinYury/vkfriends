package io.buchin.vkfriends.features.friends.viewmodel;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import io.buchin.vkfriends.models.Friend;

public class FriendsFragmentViewState {
    private List<Friend> friends;
    private boolean isFiltersButtonVisible = true;
    private boolean isLoading = true;
    private Throwable error = null;
    private FiltersState filtersState = FiltersState.NOPE;
    private SortState sortState = SortState.NOPE;

    public List<Friend> getFriends() {
        List<Friend> result = new ArrayList<>();

        if (friends != null) {
            if (filtersState == FiltersState.NOPE) {
                result.addAll(friends);
            }
            if (filtersState == FiltersState.WITH_PHOTO) {
                for (Friend friend : friends) {
                    if (friend.getFriendPhotoMax().length() > 0) {
                        result.add(friend);
                    }
                }
            }
            if (filtersState == FiltersState.WITH_PHOTO) {
                for (Friend friend : friends) {
                    if (friend.getFriendPhotoMax().length() == 0) {
                        result.add(friend);
                    }
                }
            }

            if (sortState == SortState.ASCENDING_AGE) {
                List<Friend> badBdateFriend = new ArrayList<>();
                List<Friend> goodBdateFriend = new ArrayList<>();
                List<Friend> sortResult = new ArrayList<>();

                for (Friend friend : result) {
                    String bdate = friend.getFriendBdate();
                    if (bdate.length() == 0 || bdate.split("\\.").length != 3){
                        badBdateFriend.add(friend);
                    } else {
                        goodBdateFriend.add(friend);
                    }
                }

                Collections.sort(goodBdateFriend, (firstFriend, secondFriend) -> {
                    String firstFriendBdate = firstFriend.getFriendBdate();
                    String secondFriendBdate = secondFriend.getFriendBdate();

                    SimpleDateFormat format = new SimpleDateFormat("dd.mm.yyyy", Locale.getDefault());

                    Calendar firstFriendBdateCalendar = Calendar.getInstance();
                    firstFriendBdateCalendar.set(1700, 1, 1);

                    Calendar secondFriendBdateCalendar = Calendar.getInstance();
                    secondFriendBdateCalendar.set(1700, 1, 1);

                    try {
                        Date firstFriendBdateDate = format.parse(firstFriendBdate);
                        firstFriendBdateCalendar.setTime(firstFriendBdateDate);
                    } catch (Throwable ignore) {

                    }

                    try {
                        Date secondFriendBdateDate = format.parse(secondFriendBdate);
                        secondFriendBdateCalendar.setTime(secondFriendBdateDate);
                    } catch (Throwable ignore) {

                    }

                    return firstFriendBdateCalendar.compareTo(secondFriendBdateCalendar);
                });

                sortResult.addAll(goodBdateFriend);
                sortResult.addAll(badBdateFriend);
                result = sortResult;
            }

            if (sortState == SortState.DESCENDING_AGE) {
                List<Friend> badBdateFriend = new ArrayList<>();
                List<Friend> goodBdateFriend = new ArrayList<>();
                List<Friend> sortResult = new ArrayList<>();

                for (Friend friend : result) {
                    String bdate = friend.getFriendBdate();
                    if (bdate.length() == 0 || bdate.split("\\.").length != 3){
                        badBdateFriend.add(friend);
                    } else {
                        goodBdateFriend.add(friend);
                    }
                }

                Collections.sort(goodBdateFriend, (firstFriend, secondFriend) -> {
                    String firstFriendBdate = firstFriend.getFriendBdate();
                    String secondFriendBdate = secondFriend.getFriendBdate();

                    SimpleDateFormat format = new SimpleDateFormat("dd.mm.yyyy", Locale.getDefault());

                    Calendar firstFriendBdateCalendar = Calendar.getInstance();
                    firstFriendBdateCalendar.set(1700, 1, 1);

                    Calendar secondFriendBdateCalendar = Calendar.getInstance();
                    secondFriendBdateCalendar.set(1700, 1, 1);

                    try {
                        Date firstFriendBdateDate = format.parse(firstFriendBdate);
                        firstFriendBdateCalendar.setTime(firstFriendBdateDate);
                    } catch (Throwable ignore) {

                    }

                    try {
                        Date secondFriendBdateDate = format.parse(secondFriendBdate);
                        secondFriendBdateCalendar.setTime(secondFriendBdateDate);
                    } catch (Throwable ignore) {

                    }

                    return secondFriendBdateCalendar.compareTo(firstFriendBdateCalendar);
                });

                sortResult.addAll(goodBdateFriend);
                sortResult.addAll(badBdateFriend);
                result = sortResult;
            }
        } else {
            result = null;
        }

        return result;
    }

    public boolean isLoadingVisible(){
        return isLoading && error == null;
    }

    public boolean isContentVisible(){
        return !isLoading && error == null;
    }

    public boolean isErrorVisible(){
        return !isLoading && error != null;
    }

    public void setFriends(List<Friend> friends) {
        this.friends = friends;
    }

    public boolean isFiltersButtonVisible() {
        return isFiltersButtonVisible;
    }

    public void setFiltersButtonVisible(boolean filtersButtonVisible) {
        isFiltersButtonVisible = filtersButtonVisible;
    }

    public boolean isFiltersVisible() {
        return !isFiltersButtonVisible;
    }

    public boolean isLoading() {
        return isLoading;
    }

    public void setLoading(boolean loading) {
        isLoading = loading;
    }

    public Throwable getError() {
        return error;
    }

    public void setError(Throwable error) {
        this.error = error;
    }

    public FiltersState getFiltersState() {
        return filtersState;
    }

    public void setFiltersState(FiltersState filtersState) {
        this.filtersState = filtersState;
    }

    public SortState getSortState() {
        return sortState;
    }

    public void setSortState(SortState sortState) {
        this.sortState = sortState;
    }

    @Override
    public String toString() {
        return "FriendsFragmentViewState{" +
                "friends=" + friends +
                ", isFiltersButtonVisible=" + isFiltersButtonVisible +
                ", filtersState=" + filtersState +
                ", sortState=" + sortState +
                '}';
    }

    public enum FiltersState {
        NOPE,
        WITH_PHOTO,
        WITHOUT_PHOTO
    }

    public enum SortState {
        NOPE,
        ASCENDING_AGE,
        DESCENDING_AGE
    }
}
