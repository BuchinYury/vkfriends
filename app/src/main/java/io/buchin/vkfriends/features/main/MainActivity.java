package io.buchin.vkfriends.features.main;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKCallback;
import com.vk.sdk.VKScope;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKError;

import io.buchin.vkfriends.R;
import io.buchin.vkfriends.VkFriendsApp;
import io.buchin.vkfriends.common.di.MainActivityModule;
import io.buchin.vkfriends.features.friends.FriendsFragment;

public class MainActivity extends AppCompatActivity {
    public MainActivityModule mainActivityModule;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        setTheme(R.style.AppTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainActivityModule = new MainActivityModule(((VkFriendsApp) getApplication()).appModule);

        VKSdk.wakeUpSession(this, new VKCallback<VKSdk.LoginState>() {
            @Override
            public void onResult(VKSdk.LoginState res) {
                switch (res) {
                    case LoggedOut:
                        loginInVk();
                        break;
                    case LoggedIn:
                        break;
                    case Pending:
                        navigateToFriendsFragment();
                        break;
                    case Unknown:
                        break;
                }
            }

            @Override
            public void onError(VKError error) {

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Context context = this;

        VKCallback<VKAccessToken> callback = new VKCallback<VKAccessToken>() {
            @Override
            public void onResult(VKAccessToken res) {
                navigateToFriendsFragment();
            }

            @Override
            public void onError(VKError error) {
                loginInVk();
                Toast.makeText(context, R.string.mainactiviti_toast_explain, Toast.LENGTH_SHORT).show();
            }
        };

        if (!VKSdk.onActivityResult(requestCode, resultCode, data, callback)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void loginInVk() {
        VKSdk.login(this, VKScope.FRIENDS);
    }

    private void navigateToFriendsFragment() {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.framelayout_main_fragment_container, new FriendsFragment());
        ft.commit();
    }
}
