package io.buchin.vkfriends.features.friends.viewmodel;

import java.util.ArrayList;
import java.util.List;

import io.buchin.vkfriends.usecases.GetVkFriendsUseCase;
import io.reactivex.Observable;
import io.reactivex.functions.Function;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;
import io.reactivex.subjects.Subject;

public class FriendsFragmentViewModel {
    public Subject<FriendsFragmentIntention> intentions = PublishSubject.create();
    public BehaviorSubject<FriendsFragmentViewState> viewState;

    public FriendsFragmentViewModel(
            FriendsFragmentViewState initialState,
            GetVkFriendsUseCase getVkFriendsUseCase
    ) {
        handlerViewState(initialState, getVkFriendsUseCase);
    }

    private void handlerViewState(
            FriendsFragmentViewState initialState,
            GetVkFriendsUseCase getVkFriendsUseCase
    ) {
        viewState = BehaviorSubject.createDefault(initialState);

        Observable<Function<FriendsFragmentViewState, FriendsFragmentViewState>> loadFriends =
                intentions.filter(friendsFragmentIntention ->
                        friendsFragmentIntention == FriendsFragmentIntention.screenResumed ||
                                friendsFragmentIntention == FriendsFragmentIntention.retryButtonClicked
                )
                        .flatMap(friendsFragmentIntention -> getVkFriendsUseCase.getFriends()
                                .map(friends -> (Function<FriendsFragmentViewState, FriendsFragmentViewState>) currentViewState -> {
                                    currentViewState.setLoading(false);
                                    currentViewState.setFriends(friends);
                                    return currentViewState;
                                })
                                .startWith(currentViewState -> {
                                    currentViewState.setLoading(true);
                                    currentViewState.setError(null);
                                    return currentViewState;
                                })
                                .onErrorReturn(error -> currentViewState -> {
                                    currentViewState.setLoading(false);
                                    currentViewState.setError(error);
                                    return currentViewState;
                                })
                        );

        Observable<Function<FriendsFragmentViewState, FriendsFragmentViewState>> openFiltersButtonClicked =
                intentions.filter(friendsFragmentIntention ->
                        friendsFragmentIntention == FriendsFragmentIntention.openFiltersButtonClicked)
                        .map(friendsFragmentIntention -> currentViewState -> {
                            currentViewState.setFiltersButtonVisible(false);
                            return currentViewState;
                        });

        Observable<Function<FriendsFragmentViewState, FriendsFragmentViewState>> closeFiltersButtonClicked =
                intentions.filter(friendsFragmentIntention ->
                        friendsFragmentIntention == FriendsFragmentIntention.closeFiltersButtonClicked)
                        .map(friendsFragmentIntention -> currentViewState -> {
                            currentViewState.setFiltersButtonVisible(true);
                            return currentViewState;
                        });

        Observable<Function<FriendsFragmentViewState, FriendsFragmentViewState>> filterNopeCheckedChange =
                intentions.filter(friendsFragmentIntention ->
                        friendsFragmentIntention instanceof FriendsFragmentIntention.FilterNopeCheckedChange)
                        .map(friendsFragmentIntention -> currentViewState -> {
                            boolean isFilterNopeChecked =
                                    ((FriendsFragmentIntention.FilterNopeCheckedChange) friendsFragmentIntention).isFilterNopeChecked();
                            if (isFilterNopeChecked) {
                                currentViewState.setFiltersState(FriendsFragmentViewState.FiltersState.NOPE);
                            }
                            return currentViewState;
                        });

        Observable<Function<FriendsFragmentViewState, FriendsFragmentViewState>> filterWithPhotoCheckedChange =
                intentions.filter(friendsFragmentIntention ->
                        friendsFragmentIntention instanceof FriendsFragmentIntention.FilterWithPhotoCheckedChange)
                        .map(friendsFragmentIntention -> currentViewState -> {
                            boolean isFilterWithPhotoChecked =
                                    ((FriendsFragmentIntention.FilterWithPhotoCheckedChange) friendsFragmentIntention).isFilterWithPhotoChecked();
                            if (isFilterWithPhotoChecked) {
                                currentViewState.setFiltersState(FriendsFragmentViewState.FiltersState.WITH_PHOTO);
                            }
                            return currentViewState;
                        });

        Observable<Function<FriendsFragmentViewState, FriendsFragmentViewState>> filterWithoutPhotoCheckedChange =
                intentions.filter(friendsFragmentIntention ->
                        friendsFragmentIntention instanceof FriendsFragmentIntention.FilterWithoutPhotoCheckedChange)
                        .map(friendsFragmentIntention -> currentViewState -> {
                            boolean isFilterWithoutPhotoChecked =
                                    ((FriendsFragmentIntention.FilterWithoutPhotoCheckedChange) friendsFragmentIntention).isFilterWithoutPhotoChecked();
                            if (isFilterWithoutPhotoChecked) {
                                currentViewState.setFiltersState(FriendsFragmentViewState.FiltersState.WITHOUT_PHOTO);
                            }
                            return currentViewState;
                        });

        Observable<Function<FriendsFragmentViewState, FriendsFragmentViewState>> sortNopeCheckedChange =
                intentions.filter(friendsFragmentIntention ->
                        friendsFragmentIntention instanceof FriendsFragmentIntention.SortNopeCheckedChange)
                        .map(friendsFragmentIntention -> currentViewState -> {
                            boolean isSortNopeChecked =
                                    ((FriendsFragmentIntention.SortNopeCheckedChange) friendsFragmentIntention).isSortNopeChecked();
                            if (isSortNopeChecked) {
                                currentViewState.setSortState(FriendsFragmentViewState.SortState.NOPE);
                            }
                            return currentViewState;
                        });

        Observable<Function<FriendsFragmentViewState, FriendsFragmentViewState>> sortAscendingAgeCheckedChange =
                intentions.filter(friendsFragmentIntention ->
                        friendsFragmentIntention instanceof FriendsFragmentIntention.SortAscendingAgeCheckedChange)
                        .map(friendsFragmentIntention -> currentViewState -> {
                            boolean isSortAscendingAgeChecked =
                                    ((FriendsFragmentIntention.SortAscendingAgeCheckedChange) friendsFragmentIntention).isSortAscendingAgeChecked();
                            if (isSortAscendingAgeChecked) {
                                currentViewState.setSortState(FriendsFragmentViewState.SortState.ASCENDING_AGE);
                            }
                            return currentViewState;
                        });

        Observable<Function<FriendsFragmentViewState, FriendsFragmentViewState>> sortDescendingAgeCheckedChange =
                intentions.filter(friendsFragmentIntention ->
                        friendsFragmentIntention instanceof FriendsFragmentIntention.SortDescendingAgeCheckedChange)
                        .map(friendsFragmentIntention -> currentViewState -> {
                            boolean isSortDescendingAgeChecked =
                                    ((FriendsFragmentIntention.SortDescendingAgeCheckedChange) friendsFragmentIntention).isSortDescendingAgeChecked();
                            if (isSortDescendingAgeChecked) {
                                currentViewState.setSortState(FriendsFragmentViewState.SortState.DESCENDING_AGE);
                            }
                            return currentViewState;
                        });

        List<Observable<Function<FriendsFragmentViewState, FriendsFragmentViewState>>> observablesToMerge = new ArrayList<>();
        observablesToMerge.add(loadFriends);
        observablesToMerge.add(openFiltersButtonClicked);
        observablesToMerge.add(closeFiltersButtonClicked);
        observablesToMerge.add(filterNopeCheckedChange);
        observablesToMerge.add(filterWithPhotoCheckedChange);
        observablesToMerge.add(filterWithoutPhotoCheckedChange);
        observablesToMerge.add(sortNopeCheckedChange);
        observablesToMerge.add(sortAscendingAgeCheckedChange);
        observablesToMerge.add(sortDescendingAgeCheckedChange);

        Observable.merge(observablesToMerge)
                .scan(initialState,
                        (currentViewState, stateReducer) -> stateReducer.apply(currentViewState))
                .subscribe(viewState);
    }
}
