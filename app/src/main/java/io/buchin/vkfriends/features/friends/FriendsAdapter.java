package io.buchin.vkfriends.features.friends;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.vk.sdk.api.model.VKApiUserFull;

import java.util.List;

import io.buchin.vkfriends.R;
import io.buchin.vkfriends.models.Friend;

public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.FriendsViewHolder> {
    private List<Friend> friends;

    public void setVkUsersArray(List<Friend> friends) {
        this.friends = friends;
    }

    @NonNull
    @Override
    public FriendsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_vk_friend, parent, false);

        return new FriendsViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull FriendsViewHolder friendsViewHolder, int position) {
        friendsViewHolder.bind(friends.get(position));
    }

    @Override
    public int getItemCount() {
        if (friends == null) {
            return 0;
        } else {
            return friends.size();
        }
    }

    class FriendsViewHolder extends RecyclerView.ViewHolder {
        private ImageView friendPhoto = itemView.findViewById(R.id.imageview_vk_friend_photo);
        private TextView friendName = itemView.findViewById(R.id.textview_friend_name);
        private LinearLayout friendBdateView = itemView.findViewById(R.id.textile_friend_bdate_view);
        private TextView friendBdate = itemView.findViewById(R.id.textile_friend_bdate);

        public FriendsViewHolder(@NonNull View itemView) {
            super(itemView);
        }

        @SuppressLint("SetTextI18n")
        public void bind(Friend friend) {
            if (friend.getFriendPhotoMax().length() > 0) {
                Picasso.get()
                        .load(friend.getFriendPhotoMax())
                        .into(friendPhoto);
            }

            friendName.setText(friend.getFriendName() + " " + friend.getFriendSurname());

            if (friend.getFriendBdate().length() > 0) {
                friendBdateView.setVisibility(View.VISIBLE);
                friendBdate.setText(friend.getFriendBdate());
            } else {
                friendBdateView.setVisibility(View.INVISIBLE);
            }

        }
    }
}
