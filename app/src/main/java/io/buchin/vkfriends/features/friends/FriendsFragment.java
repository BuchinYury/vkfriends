package io.buchin.vkfriends.features.friends;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.List;

import io.buchin.vkfriends.R;
import io.buchin.vkfriends.common.di.FriendsFragmentModule;
import io.buchin.vkfriends.features.friends.viewmodel.FriendsFragmentIntention;
import io.buchin.vkfriends.features.friends.viewmodel.FriendsFragmentViewModel;
import io.buchin.vkfriends.features.friends.viewmodel.FriendsFragmentViewState;
import io.buchin.vkfriends.features.main.MainActivity;
import io.buchin.vkfriends.models.Friend;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;

public class FriendsFragment extends Fragment {
    private FriendsFragmentViewModel friendsFragmentViewModel;
    private CompositeDisposable disposeBag;
    private FriendsAdapter friendsAdapter;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FriendsFragmentModule friendsFragmentModule =
                new FriendsFragmentModule(((MainActivity) getActivity()).mainActivityModule);
        friendsFragmentViewModel = friendsFragmentModule.friendsFragmentViewModel;
        disposeBag = friendsFragmentModule.disposeBag;
        friendsAdapter = friendsFragmentModule.friendsAdapter;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_vk_friends, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        handleFiltersView();
        handleFilters();
        handleFriends();
        handleLoading();
        handleContent();
        handleError();
        handleSort();
    }

    @Override
    public void onResume() {
        super.onResume();
        friendsFragmentViewModel.intentions.onNext(FriendsFragmentIntention.screenResumed);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        disposeBag.dispose();
    }

    private void handleLoading() {
        FrameLayout loadingView = getView().findViewById(R.id.framelayout_loading);

        Disposable isLoadingDisposable = friendsFragmentViewModel.viewState
                .map(FriendsFragmentViewState::isLoadingVisible)
                .distinctUntilChanged()
                .subscribe(isLoadingVisible ->
                        loadingView.setVisibility(isLoadingVisible ? View.VISIBLE : View.GONE));

        disposeBag.add(isLoadingDisposable);
    }

    private void handleError() {
        LinearLayout errorView = getView().findViewById(R.id.framelayout_error);
        TextView errorMessageView = getView().findViewById(R.id.textview_error_message);
        Button retryButton = getView().findViewById(R.id.button_error_retry);

        retryButton.setOnClickListener(view ->
                friendsFragmentViewModel.intentions.onNext(FriendsFragmentIntention.retryButtonClicked));

        Disposable isErrorDisposable = friendsFragmentViewModel.viewState
                .map(FriendsFragmentViewState::isErrorVisible)
                .distinctUntilChanged()
                .subscribe(isErrorVisible ->
                        errorView.setVisibility(isErrorVisible ? View.VISIBLE : View.GONE));

        Disposable errorMessageDisposable = friendsFragmentViewModel.viewState
                .filter(viewState -> viewState.getError() != null)
                .subscribe(viewState -> {
                    String errorMessage = viewState.getError().getMessage() == null
                            ? getString(R.string.default_error_message)
                            : viewState.getError().getMessage();
                    errorMessageView.setText(errorMessage);
                });

        disposeBag.add(isErrorDisposable);
        disposeBag.add(errorMessageDisposable);
    }

    private void handleContent() {
        FrameLayout content = getView().findViewById(R.id.framelayout_content);

        Disposable isErrorDisposable = friendsFragmentViewModel.viewState
                .map(FriendsFragmentViewState::isContentVisible)
                .distinctUntilChanged()
                .subscribe(isContentVisible ->
                        content.setVisibility(isContentVisible ? View.VISIBLE : View.GONE));

        disposeBag.add(isErrorDisposable);
    }

    private void handleFriends() {
        RecyclerView friendsRecyclerView = getView().findViewById(R.id.recyclerview_friends);
        friendsRecyclerView.setAdapter(friendsAdapter);

        friendsRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), 1, false));

        Disposable friendsDisposable = friendsFragmentViewModel.viewState
                .subscribe(friendsFragmentViewState -> {
                    List<Friend> friends = friendsFragmentViewState.getFriends();
                    Log.e("friends", friends == null ? "null" : friends.toString());
                    if (friends != null) {
                        friendsAdapter.setVkUsersArray(friends);
                        friendsAdapter.notifyDataSetChanged();
                    }
                });

        disposeBag.add(friendsDisposable);
    }

    private void handleFiltersView() {
        ImageView openFiltersButton = getView().findViewById(R.id.imageview_vk_friends_open_filters);
        ImageView closeFiltersButton = getView().findViewById(R.id.imageview_vk_friends_close_filters);
        LinearLayout filtersView = getView().findViewById(R.id.linearlayout_vk_friends_filters);

        openFiltersButton.setOnClickListener(view ->
                friendsFragmentViewModel.intentions.onNext(FriendsFragmentIntention.openFiltersButtonClicked));

        closeFiltersButton.setOnClickListener(view ->
                friendsFragmentViewModel.intentions.onNext(FriendsFragmentIntention.closeFiltersButtonClicked));

        Disposable filtersButtonVisibilityDisposable = friendsFragmentViewModel.viewState
                .subscribe(friendsFragmentViewState -> {
                    int filtersButtonVisibility =
                            friendsFragmentViewState.isFiltersButtonVisible() ? View.VISIBLE : View.GONE;
                    openFiltersButton.setVisibility(filtersButtonVisibility);
                });

        Disposable filtersVisibilityDisposable = friendsFragmentViewModel.viewState
                .map(FriendsFragmentViewState::isFiltersVisible)
                .distinctUntilChanged()
                .subscribe(isFiltersVisible -> {
                    if (isFiltersVisible) {
                        filtersView.setVisibility(View.VISIBLE);
                        Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.open_filters_animation);
                        filtersView.startAnimation(animation);
                    } else {
                        Animation animation = AnimationUtils.loadAnimation(getContext(), R.anim.close_filters_animation);
                        filtersView.startAnimation(animation);

                        animation.setAnimationListener(new Animation.AnimationListener() {
                            @Override
                            public void onAnimationStart(Animation animation) {
                                closeFiltersButton.setClickable(false);
                            }

                            @Override
                            public void onAnimationEnd(Animation animation) {
                                filtersView.setVisibility(View.GONE);
                                closeFiltersButton.setClickable(true);
                            }

                            @Override
                            public void onAnimationRepeat(Animation animation) {

                            }
                        });
                    }
                });

        disposeBag.add(filtersButtonVisibilityDisposable);
        disposeBag.add(filtersVisibilityDisposable);
    }

    private void handleFilters() {
        RadioButton filterNope = getView().findViewById(R.id.radiobutton_vk_friends_filter_nope);
        RadioButton filterWithPhoto = getView().findViewById(R.id.radiobutton_vk_friends_filter_with_photo);
        RadioButton filterWithoutPhoto = getView().findViewById(R.id.radiobutton_vk_friends_filter_without_photo);

        filterNope.setOnCheckedChangeListener((compoundButton, b) ->
                friendsFragmentViewModel.intentions.onNext(new FriendsFragmentIntention.FilterNopeCheckedChange(b)));

        filterWithPhoto.setOnCheckedChangeListener((compoundButton, b) ->
                friendsFragmentViewModel.intentions.onNext(new FriendsFragmentIntention.FilterWithPhotoCheckedChange(b)));

        filterWithoutPhoto.setOnCheckedChangeListener((compoundButton, b) ->
                friendsFragmentViewModel.intentions.onNext(new FriendsFragmentIntention.FilterWithoutPhotoCheckedChange(b)));
    }

    private void handleSort() {
        RadioButton sortNope = getView().findViewById(R.id.radiobutton_vk_friends_sort_nope);
        RadioButton sortAscendingAge = getView().findViewById(R.id.radiobutton_vk_friends_sort_ascending_age);
        RadioButton sortDescendingAge = getView().findViewById(R.id.radioButtonKitten_descending_age);

        sortNope.setOnCheckedChangeListener((compoundButton, b) ->
                friendsFragmentViewModel.intentions.onNext(new FriendsFragmentIntention.SortNopeCheckedChange(b)));

        sortAscendingAge.setOnCheckedChangeListener((compoundButton, b) ->
                friendsFragmentViewModel.intentions.onNext(new FriendsFragmentIntention.SortAscendingAgeCheckedChange(b)));

        sortDescendingAge.setOnCheckedChangeListener((compoundButton, b) ->
                friendsFragmentViewModel.intentions.onNext(new FriendsFragmentIntention.SortDescendingAgeCheckedChange(b)));
    }
}
