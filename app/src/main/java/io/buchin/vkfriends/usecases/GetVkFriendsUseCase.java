package io.buchin.vkfriends.usecases;

import com.vk.sdk.api.VKApi;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;
import com.vk.sdk.api.model.VKApiUserFull;
import com.vk.sdk.api.model.VKUsersArray;

import java.util.ArrayList;
import java.util.List;

import io.buchin.vkfriends.models.Friend;
import io.reactivex.Observable;

public class GetVkFriendsUseCase {
    public Observable<List<Friend>> getFriends() {
        return Observable.create(emitter -> VKApi.friends()
                .get(VKParameters.from(VKApiConst.FIELDS, "id,first_name,last_name,photo_max,bdate"))
                .executeWithListener(new VKRequest.VKRequestListener() {
                    @Override
                    public void onComplete(VKResponse response) {
                        List<Friend> friendList = new ArrayList<>();
                        VKUsersArray friendsResponse = (VKUsersArray) response.parsedModel;
                        if (friendsResponse != null && friendsResponse.getCount() > 0) {
                            for (VKApiUserFull userFull : friendsResponse) {
                                Friend friend = new Friend(userFull.first_name,
                                        userFull.last_name,
                                        userFull.photo_max,
                                        userFull.bdate);

                                friendList.add(friend);
                            }
                        }
                        emitter.onNext(friendList);
                        emitter.onComplete();
                    }

                    @Override
                    public void onError(VKError error) {
                        emitter.onError(new Throwable(error.toString()));
                    }

                    @Override
                    public void onProgress(VKRequest.VKProgressType progressType,
                                           long bytesLoaded,
                                           long bytesTotal) {
                    }

                    @Override
                    public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
                    }
                }));
    }
}
