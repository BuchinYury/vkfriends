package io.buchin.vkfriends.common.di;

import io.buchin.vkfriends.usecases.GetVkFriendsUseCase;

public class MainActivityModule {
    public GetVkFriendsUseCase getVkFriendsUseCase = new GetVkFriendsUseCase();

    public MainActivityModule(AppModule appModule) {
    }
}
