package io.buchin.vkfriends.common.di;

import io.buchin.vkfriends.features.friends.FriendsAdapter;
import io.buchin.vkfriends.features.friends.viewmodel.FriendsFragmentViewModel;
import io.buchin.vkfriends.features.friends.viewmodel.FriendsFragmentViewState;
import io.buchin.vkfriends.usecases.GetVkFriendsUseCase;
import io.reactivex.disposables.CompositeDisposable;

public class FriendsFragmentModule {
    public FriendsFragmentViewModel friendsFragmentViewModel;
    public CompositeDisposable disposeBag = new CompositeDisposable();
    public FriendsAdapter friendsAdapter = new FriendsAdapter();

    public FriendsFragmentModule(MainActivityModule mainActivityModule) {
        GetVkFriendsUseCase getVkFriendsUseCase = mainActivityModule.getVkFriendsUseCase;
        FriendsFragmentViewState initialState = new FriendsFragmentViewState();
        friendsFragmentViewModel = new FriendsFragmentViewModel(initialState, getVkFriendsUseCase);
    }
}
